/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
jQuery(document).ready(function() {
    var coordinatesInput = jQuery('#slider_image_map_coordinates'),
        imageLocation = jQuery('input#slider_image_map_image_location').val();

    coordinatesInput.attr('data-image-url', imageLocation);
    coordinatesInput.canvasAreaDraw({
        imgUrl: imageLocation
    });
});
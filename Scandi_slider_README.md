Scandi_Slider
===================
> Module allows admin to create multiple sliders with images and manage them within Magento backend.
  Additional feature of this extension is a possibility to create image maps with links to product pages, and show
  product information on hover.

Installation
------------
* For manual installation copy all folders to project root.
* Clear cache and extension should be installed.

Testing installation success
----------------------------
* After extension installation, store admin can add new image slider in backend under CMS -> Slider. There can be
multiple sliders and each slider may have many images.

Extension Configuration
----------------------------
CMS -> Slider

Depends
----------------------------
*   jQuery Library or similar module

Available Functionality list
----------------------------
* Show/Hide slider navigation
* Set animation speed
* Set number of slides to display/scroll (also on tablets and mobile)
* Set specific store view for slides
* Show/Hide slide Title, Text
* Choose slide Title and Text position
* Edit slide Text in wyswig
* Set image or video slide
* Add hyperlink for slide
* Show/Hide hyperlink
* Set image maps for slide (with links to product pages)

Todo
----------------------------
* Stop a video when slide changes

Developer Notes
----------------------------
* Add slider within layout files (theme local.xml or other):


    <block type="scandi_slider/slider" name="slider_name">
        <action method="setSliderId">
            <slider_id>slider_identifier</slider_id>
        </action>
    </block>

* Add slider within layout files (theme local.xml or other):


    {{block type="scandi_slider/slider" name="slider_name" slider_id="slider_identifier"}}

* Add slider directly into template:


    <?php echo $this->getLayout()->createBlock('scandi_slider/slider')->setSliderId('slider_identifier')->toHtml(); ?>

* Slider js settings:


    auto                : true,
    circular            : false,
    delay               : 5,
    animationDuration   : 1000,
    showItemNav         : true,
    showPrevNextNav     : true,
    width               : '100%',
    height              : '400px'
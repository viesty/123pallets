/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
jQuery(document).ready(function() {
    var mapArea = jQuery('.image-maps area');

    mapArea.each(function(){
        var area = jQuery(this),
            productBlockId = '#' + jQuery(this).attr('data-product-block-id'),
            productBlock = jQuery('.product-blocks').find(productBlockId),
            productUrl = productBlock.attr('data-product-url');

        area.attr('href', productUrl);

        area.mousemove(function(e){
            productBlock.offset({
                'top'       : e.pageY - 50 - productBlock.height(),
                'left'      : e.pageX - 50 - productBlock.width()
            })
        });

        area.mouseover(function() {
            productBlock.css({
                'display'   : 'block',
                'position'  : 'absolute'
            })
        });

        area.mouseleave(function() {
            productBlock.css('display', 'none');
        });
    });
});
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
jQuery(document).ready(function() {
    jQuery('img[usemap]').rwdImageMaps();
});
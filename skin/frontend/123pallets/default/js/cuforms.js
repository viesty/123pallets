/**
 * CuForms - Custom Form Element Plugin
 */
var CuForms = Class.create();

CuForms.prototype = {

    initialize : function (elementSelector, options) {

        Object.extend(this.options, options);

        var elements = $$(elementSelector),
            elementsLength = elements.length;

        this.options.elementArraySet.push({
            element : elementSelector,
            options : options
        });

        if (!elementsLength) {
            return;
        }

        elements.each(function (element) {
            var parentElement = element.up();

            if ((element.offsetWidth === 0 && element.offsetHeight === 0)
                || element.hasClassName(this.options.customElementClass)) {
                return;
            }

            if ((!element.readAttribute('multiple') && element.match('select'))
                || element.match(':checkbox') || element.match(':radio')) {

                if (parentElement.match('td') || parentElement.match('th')) {
                    element.wrap(new Element('span'));
                }

                // Variables for element's class names and statuses
                var elementClassNameByTag = '',
                    elementClassNameByStatus = '';

                parentElement.addClassName(this.options.customParentClass);
                element.addClassName(this.options.customElementClass)
                    .setStyle({opacity : 0});

                // Checking element's statuses and adding class names
                if (element.match(':radio')) {
                    elementClassNameByTag = this.options.customRadioClass + ' ';
                }

                if (element.match(':checkbox')) {
                    elementClassNameByTag = this.options.customCheckboxClass + ' ';
                }

                if (element.match('select')) {
                    elementClassNameByTag = this.options.customSelectClass + ' ';
                }

                if (element.checked) {
                    elementClassNameByStatus += this.options.activeElementClass + ' ';
                }

                if (element.disabled) {
                    elementClassNameByStatus += this.options.disabledElementClass + ' ';
                }

                // Fix Webkit for MAC select height
                /*if (navigator.appVersion.indexOf("Mac") !== -1 && element.is('select')) {
                 element.css({'-webkit-appearance' : 'menulist-button'});
                 }*/

                // Create new element and add it before original
                var customElement = new Element('div', {'class' : this.options.bckElementClass
                    + ' ' + elementClassNameByTag + elementClassNameByStatus + this.options.addCustomClass});

                customElement.setStyle({
                    width : element.getWidth() + 'px',
                    height : element.getHeight() + 'px',
                    top : element.offsetTop + 'px',
                    left : element.offsetLeft + 'px'
                });

                if (element.match('select')) {
                    var selectOuter = new Element('div', {'class' : this.options.selectOuterElemClass}),
                        selectInner = new Element('div', {'class' : this.options.selectInnerElemClass});

                    selectInner.setStyle({
                        height : element.getHeight() + 'px'
                    }).update(element.select('option:selected')[0].innerHTML);

                    selectOuter.setStyle({
                        height : element.getHeight() + 'px'
                    }).update(selectInner);

                    customElement.update(selectOuter);
                }

                element.insert({'before' : customElement});

                // Mouse events for hover classes
                element.observe('mouseover', function () {
                    element.previous().addClassName(this.options.hoverElementClass);
                }.bind(this));

                element.observe('mouseleave', function () {
                    element.previous().removeClassName(this.options.hoverElementClass);
                }.bind(this));
            }
        }.bind(this));

        // Add listeners to all potential click and change elements
        if (!this.options.eventsAttached) {
            document.observe('click', function (event) {
                if (event.findElement(this.options.potentialClick)) {
                    this.event();
                }
            }.bind(this));

            document.observe('change', function (event) {
                if (event.findElement(this.options.potentialChange)) {
                    this.event();
                }
            }.bind(this));

            this.options.eventsAttached = true;
        }
    },

    event : function () {

        $$('.' + this.options.customElementClass).each(function (element) {
            var customElement = element.previous();

            if (!element.checked) {
                customElement.removeClassName(this.options.activeElementClass);
            } else {
                customElement.addClassName(this.options.activeElementClass);
            }

            if (!element.disabled) {
                customElement.removeClassName(this.options.disabledElementClass);
            } else {
                customElement.addClassName(this.options.disabledElementClass);
            }

            if (element.offsetWidth === 0 && element.offsetHeight === 0) {
                customElement.addClassName(this.options.hiddenElementClass);
            } else {
                customElement.removeClassName(this.options.hiddenElementClass);
            }

            if (element.match('select')) {
                customElement.select('.' + this.options.selectInnerElemClass)[0]
                    .update(element.select('option:selected')[0].innerHTML);
            }
        }.bind(this));
    },

    refresh : function () {

        var elementArraySet = this.options.elementArraySet;
        this.options.elementArraySet = [];

        elementArraySet.each(function (arrayItem) {
            this.initialize(arrayItem.element, arrayItem.options);
        }.bind(this));
    },

    destroy : function (keepStoredElements) {

        $$('.' + this.options.customElementClass).each(function (element) {
            element.removeClassName(this.options.customElementClass).setStyle({opacity : 1});
            element.up().removeClassName(this.options.customParentClass);
            element.previous().remove();

            element.stopObserving('mouseover');
            element.stopObserving('mouseleave');
            //element.css({'-webkit-appearance' : ''});
        }.bind(this));

        document.stopObserving('click');
        document.stopObserving('change');

        if (!keepStoredElements) {
            this.options.elementArraySet = [];
        }

        this.options.eventAttached = false;
    },

    options : {
        bckElementClass      :   'cu-forms-custom-over',
        customParentClass    :   'cu-forms-custom-parent',
        customElementClass   :   'cu-forms-custom-element',

        customRadioClass     :   'cu-forms-radio',
        customSelectClass    :   'cu-forms-select',
        customCheckboxClass  :   'cu-forms-checkbox',

        hoverElementClass    :   'cu-forms-hover',
        activeElementClass   :   'cu-forms-active',
        hiddenElementClass   :   'cu-forms-hidden',
        disabledElementClass :   'cu-forms-disabled',

        selectInnerElemClass :   'cu-forms-select-inner',
        selectOuterElemClass :   'cu-forms-select-outer',

        addCustomClass       :   '',
        elementArraySet      :   [],

        potentialClick       :   'input:radio, input:checkbox, *[onclick], button',
        potentialChange      :   'select'
    }
};
var cuForms = {};
document.observe('dom:loaded', function () {
    cuForms = new CuForms('select');
});
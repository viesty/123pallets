<?php
/**
 * @category    Scandi
 * @package     Scandi_Jquery
 * @author      Scandiweb <info@scandiweb.com>
 */
class Scandi_Jquery_Model_Observer
{
    /**
     * Include jQuery in all frontend pages before all other js files
     *
     * @param Varien_Event_Observer $observer
     */
    public function includeJquery (Varien_Event_Observer $observer)
    {
        if ($observer['block'] instanceof Mage_Page_Block_Html_Head) {
            $observer['block']->addItem('js', 'scandi/jquery/jquery.js');
        }
    }
}
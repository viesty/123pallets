<?php
class Scandi_Frontnav_Model_Categories
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
    	$category = Mage::getModel('catalog/category');
		$tree = $category->getTreeModel();
		$tree->load();
		
		$ids = $tree->getCollection()->getAllIds();
		$arr = array();
		
		if ($ids){
			foreach ($ids as $id){
				$cat = Mage::getModel('catalog/category');
				$cat->load($id);
				if($cat->getLevel() < 2) continue;
				$arr[] = array('value' => $id, 'label' => $cat->getName());
			}
		} 
    
        return $arr;
    }

}
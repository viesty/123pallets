<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Kriss Andrejevs <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Add column for youtube iframe code
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'iframe',
        'VARCHAR(1000)'
    );

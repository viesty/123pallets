<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Kriss Andrejevs <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Add column for hyperlink url if slide is hyperlink
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'slide_link',
        'VARCHAR(250)'
    );
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 * @author      Ramona Cunska <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Add field for slider animation delay
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slide_delay',
        'TEXT null DEFAULT null'
    );

//Add field for slider image position
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'image_position',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable'  => false,
            'default'   => '0',
            'comment'   => 'Slider Image Position'
        )
    );
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Kriss Andrejevs <info@scandiweb.com>
 * @author      Livija Golosujeva <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Add field for image map product id
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image_map'),
        'product_id',
        'INTEGER null DEFAULT null'
    );

//Add field in DB for slide animation speed
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'animation_speed',
        'VARCHAR(250)'
    );

//Add field in DB for slide display speed
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slide_speed',
        'VARCHAR(250)'
    );

//Drop slide delay column (replaced with slide speed)
$installer->getConnection()
    ->dropColumn(
        $installer->getTable('scandi_slider/slider'), 'slide_delay'
    );

//Add field in DB for slide animation speed
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'animation_speed',
        'VARCHAR(250)'
    );

//Add field in DB for slide display speed
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slide_speed',
        'VARCHAR(250)'
    );

//Drop slide delay column (replaced with slide speed)
$installer->getConnection()
    ->dropColumn(
        $installer->getTable('scandi_slider/slider'), 'slide_delay'
    );

//Add column for slider image position
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'image_position',
        array(
            'type'     => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable' => false,
            'default'  => '0',
            'comment'  => 'Slider Image Position'
        )
    );

//Add column for lazy loading enable/disable option
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'lazy_load',
        'INTEGER'
    );

//Add column for hyperlink url if slide is hyperlink
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'slide_link',
        'VARCHAR(250)'
    );

//Add column for slide title
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'slide_title',
        'VARCHAR(250)'
    );

//Add column for slide text
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'slide_text',
        'VARCHAR(1000)'
    );

//Add column for slides to display
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_display',
        'INTEGER'
    );

//Add column for slides to scroll
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_scroll',
        'INTEGER'
    );

//Add column for slides to display on tablet devices
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_display_tablet',
        'INTEGER'
    );

//Add column for slides to scroll on tablet devices
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_scroll_tablet',
        'INTEGER'
    );

//Add column for slides to display on mobile devices
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_display_mobile',
        'INTEGER'
    );

//Add column for slides to scroll on mobile devices
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_scroll_mobile',
        'INTEGER'
    );

//Add column for youtube iframe code
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'iframe',
        'VARCHAR(1000)'
    );

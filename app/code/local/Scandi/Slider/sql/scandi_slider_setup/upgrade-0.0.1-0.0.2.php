<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

/* Update table "scandi_slider/slider" */
//Add Field for slider menu configuration
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'show_menu',
        'TINYINT null DEFAULT 1'
    );
//Add field for slider navigation configuration
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'show_navigation',
        'TINYINT null DEFAULT 1'
    );
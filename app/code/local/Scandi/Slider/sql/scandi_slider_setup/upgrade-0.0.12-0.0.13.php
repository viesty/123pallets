<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Ramona Cunska <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Add slide foreground text position column
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'slide_text_position',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'length'    => 6,
            'nullable'  => false,
            'default'   => '0',
            'comment'   => 'Slide Text Block Position'
        )
    );

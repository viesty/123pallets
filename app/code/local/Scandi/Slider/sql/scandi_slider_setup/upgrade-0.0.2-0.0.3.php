<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

/* Create table "scandi_slider/image_map" */
$table = $installer->getConnection()
    ->newTable(
        $installer->getTable('scandi_slider/image_map')
    )
    ->addColumn(
        'id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'ID'
    )
    ->addColumn(
        'image_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => false,
        ), 'Slider Image ID'
    )
    ->addColumn(
        'map_title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ), 'Image Map Title'
    )
    ->addColumn(
        'coordinates', Varien_Db_Ddl_Table::TYPE_TEXT, Varien_Db_Ddl_Table::DEFAULT_TEXT_SIZE, array(
            'nullable'  => false,
        ), 'Image Map Coordinates'
    )
    ->addColumn(
        'is_active', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
            'nullable'  => false,
            'default'   => '1',
        ), 'Is Slider Image Map Active'
    )
    ->addForeignKey(
        $installer->getFkName(
            'scandi_slider/image_map', 'image_id', 'scandi_slider/image', 'id'
        ),
        'image_id', $installer->getTable('scandi_slider/image'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Scandi Slider Image Map Table');

$installer->getConnection()->createTable($table);

//Add field for slider animation delay
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slide_delay',
        'TEXT null DEFAULT null'
    );
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Kriss Andrejevs <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Add column for slides to display on tablet devices
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_display_tablet',
        'INTEGER'
    );

//Add column for slides to scroll on tablet devices
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_scroll_tablet',
        'INTEGER'
    );

//Add column for slides to display on mobile devices
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_display_mobile',
        'INTEGER'
    );

//Add column for slides to scroll on mobile devices
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'slides_to_scroll_mobile',
        'INTEGER'
    );
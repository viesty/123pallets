<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Kriss Andrejevs <info@scandiweb.com>
 * @author      Ramona Cunska <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Update slider image text type and length
$installer->getConnection()
    ->modifyColumn(
        $installer->getTable('scandi_slider/image'),
        'slide_text',
        array(
            'type'   => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => '2M'
        )
    );
//Add column for hyperlink text for slider images
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'anchor_text',
        'VARCHAR(1000)'
    );

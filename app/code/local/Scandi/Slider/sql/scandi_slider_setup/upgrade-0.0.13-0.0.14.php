<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Ramona Cunska <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Add slider image store table
$table = $installer->getConnection()
    ->newTable(
        $installer->getTable('scandi_slider/image_store')
    )
    ->addColumn(
        'image_id', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
            'nullable'  => false,
            'primary'   => true,
        ), 'Image ID'
    )
    ->addColumn(
        'store_id', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Store ID'
    )
    ->addIndex(
        $installer->getIdxName('scandi_slider/image_store', array('store_id')), array('store_id')
    )
    ->addForeignKey(
        $installer->getFkName(
            'scandi_slider/image_store', 'image_id', 'scandi_slider/image', 'id'
        ),
        'image_id', $installer->getTable('scandi_slider/image'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(
            'scandi_slider/image_store', 'store_id', 'core/store', 'store_id'
        ),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Scandi Slider Image To Store Linkage Table');

$installer->getConnection()->createTable($table);

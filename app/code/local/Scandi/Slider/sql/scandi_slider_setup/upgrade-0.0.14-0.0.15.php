<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Ramona Cunska <info@scandiweb.com>
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

//Create associated slider attribute for categories
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'slider_id', array(
    'group'    => 'Display Settings',
    'type'     => 'varchar',
    'label'    => 'Associated Slider',
    'input'    => 'select',
    'source'   => 'scandi_slider/category_attribute_source_slider',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
    'default'  => 0
));
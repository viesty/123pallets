<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Kriss Andrejevs <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Add column for slide title
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'slide_title',
        'VARCHAR(250)'
    );

//Add column for slide text
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/image'),
        'slide_text',
        'VARCHAR(1000)'
    );
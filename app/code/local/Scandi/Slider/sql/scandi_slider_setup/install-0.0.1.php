<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

/* Create table "scandi_slider/slider" */
$table = $installer->getConnection()
    ->newTable(
        $installer->getTable('scandi_slider/slider')
    )
    ->addColumn(
        'id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'ID'
    )
    ->addColumn(
        'block_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ), 'Slider Block ID'
    )
    ->addColumn(
        'title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ), 'Slider Title'
    )
    ->addColumn(
        'is_active', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
            'nullable'  => false,
            'default'   => '1',
        ), 'Is Slider Active'
    )
    ->addColumn(
        'slider_width', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => false,
            'default'   => '0'
        ), 'Slider Width'
    )
    ->addColumn(
        'slider_height', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => false,
            'default'   => '0'
        ), 'Slider Height'
    )
    ->setComment('Scandi Slider Slider Table');

$installer->getConnection()->createTable($table);

/* Create table "scandi_slider/image" */
$table = $installer->getConnection()
    ->newTable(
        $installer->getTable('scandi_slider/image')
    )
    ->addColumn(
        'id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'ID'
    )
    ->addColumn(
        'slider_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => false,
        ), 'Slider ID'
    )
    ->addColumn(
        'image_title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ), 'Image Title'
    )
    ->addColumn(
        'location', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ), 'Image Location'
    )
    ->addColumn(
        'image_is_active', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
            'nullable'  => false,
            'default'   => '1',
        ), 'Is Slider Image Active'
    )
    ->addForeignKey(
        $installer->getFkName(
            'scandi_slider/image', 'slider_id', 'scandi_slider/slider', 'id'
        ),
        'slider_id', $installer->getTable('scandi_slider/slider'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Scandi Slider Image Table');

$installer->getConnection()->createTable($table);
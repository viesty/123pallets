<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
  * @author      Kriss Andrejevs <info@scandiweb.com>
 * @author      Ramona Cunska <info@scandiweb.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

//Add column for lazy loading enable/disable option
$installer->getConnection()
    ->addColumn(
        $installer->getTable('scandi_slider/slider'),
        'lazy_load',
        'INTEGER'
    );
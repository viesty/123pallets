<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Slider extends Mage_Core_Block_Template
{
    /**
     * Prepare template for slider
     *
     * @return Scandi_Slider_Block_Slider
     */
    protected function _prepareLayout()
    {
        $this->setTemplate('scandi/slider/slider.phtml');
    }

    /**
     * Return loaded slider
     *
     * @return bool | Scandi_Slider_Model_Slider
     */
    public function getSlider()
    {
        if ($sliderId = $this->getData('slider_id')) {
            $slider = Mage::getModel('scandi_slider/slider')
                ->load($sliderId, 'block_id');

            if ($slider->getIsActive()) {
                return $slider;
            }
        }

        return false;
    }

    /**
     * Return filtered slider image collection
     *
     * @return Scandi_Slider_Model_Resource_Image_Collection
     */
    protected function _getSliderImageCollection()
    {
        return Mage::getModel('scandi_slider/image')->getCollection()
            ->addSliderFilter($this->getSlider())
            ->setOrder('image_position', Varien_Db_Select::SQL_ASC)
            ->addStatusFilter();
    }

    /**
     * Return Slider Image Collection
     *
     * @return Scandi_Slider_Model_Resource_Image_Collection
     */
    public function getSliderImages()
    {
        return $this->_getSliderImageCollection();
    }

    /**
     * Return url for slider image folder in media folder
     *
     * @return string
     */
    public function getSliderMediaFolder()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'slider' . DS ;
    }

    /**
     * Return image map collection
     *
     * @return Scandi_Slider_Model_Resource_Image_Map_Collection
     */
    protected function _getImageMapCollection()
    {
        return Mage::getModel('scandi_slider/image_map')->getCollection();
    }

    /**
     * Return image map collection filtered by slider
     *
     * @param int | Scandi_Slider_Model_Slider $slider
     * @return Scandi_Slider_Model_Resource_Image_Map_Collection
     */
    protected function _getSliderImageMapCollection($slider)
    {
        return $this->_getImageMapCollection()
            ->addSliderFilter($slider)
            ->addStatusFilter();
    }

    /**
     * Return filtered image map collection
     *
     * @param int | Scandi_Slider_Model_Image $image
     * @return Scandi_Slider_Model_Resource_Image_Map_Collection
     */
    protected function getFilteredImageMapCollection($image)
    {
        return $this->_getImageMapCollection()
            ->addImageFilter($image)
            ->addStatusFilter();
    }

    /**
     * Return image map collection
     *
     * @param int | Scandi_Slider_Model_Image $image
     * @return Scandi_Slider_Model_Resource_Image_Map_Collection
     */
    public function getImageMaps($image)
    {
        return $this->getFilteredImageMapCollection($image);
    }

    /**
     * Get all image maps product ids
     *
     * @return array
     */
    public function getProductIds()
    {
        $imageMaps = $this->_getSliderImageMapCollection($this->getSlider()->getId());
        $productIds = array();

        foreach ($imageMaps as $map) {
            array_push($productIds, $map->getProductId());
        }

        $productIds = array_filter($productIds);

        return array_unique($productIds);
    }

    /**
     * Create image map product info block
     *
     * @return Scandi_Slider_Block_Slider | bool
     */
    public function getProductBlocks()
    {
        $id = $this->getProductIds();
        $sliderId = $this->getSlider()->getId();

        if (count($id) > 0) {
            return $this->getLayout()->createBlock('scandi_slider/slider')
                ->setTemplate('scandi/slider/image/map/product.phtml')
                ->setProductId($id)
                ->setSliderId($sliderId)
                ->toHtml();
        } else {
            return false;
        }
    }

    /**
     * Prepare product collection for frontend
     * @param string $id
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function _prepareCollection($id)
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('entity_id', $id)
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addAttributeToSelect(array('name', 'url_path', 'small_image'));

        return $collection;
    }

    /**
     * Return product collection
     *
     * @param string $id
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProduct($id)
    {
        return $this->_prepareCollection($id);
    }

    /**
     * Return product price block
     *
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Block_Product_List
     */
    public function getProductPrice($product)
    {
        return $this->getLayout()->createBlock('catalog/product_list')->getPriceHtml($product, true);
    }
}
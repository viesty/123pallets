<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
        implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        /* @var $model Scandi_Slider_Model_Slider */
        $model = Mage::registry('slider_slider');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('slider_');

        $fieldset = $form->addFieldset('base_fieldset', array(
                'legend' => Mage::helper('scandi_slider')->__('General Information'))
        );

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('block_id', 'text', array(
            'name'      => 'block_id',
            'label'     => Mage::helper('scandi_slider')->__('Slider Block ID'),
            'title'     => Mage::helper('scandi_slider')->__('Slider Block ID'),
            'required'  => true,
            'class'     => 'validate-xml-identifier',
        ));

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => Mage::helper('scandi_slider')->__('Title'),
            'title'     => Mage::helper('scandi_slider')->__('Title'),
            'required'  => true
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('scandi_slider')->__('Status'),
            'title'     => Mage::helper('scandi_slider')->__('Slider Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('scandi_slider')->__('Enabled'),
                '0' => Mage::helper('scandi_slider')->__('Disabled'),
            ),
        ));

        $fieldset->addField('show_menu', 'select', array(
            'label'     => Mage::helper('scandi_slider')->__('Slider Menu Status'),
            'title'     => Mage::helper('scandi_slider')->__('Slider Menu Status'),
            'name'      => 'show_menu',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('scandi_slider')->__('Enabled'),
                '0' => Mage::helper('scandi_slider')->__('Disabled'),
            ),
            'note'      => 'If Enabled, will show menu with button for each slide'
        ));

        $fieldset->addField('show_navigation', 'select', array(
            'label'     => Mage::helper('scandi_slider')->__('Slider Navigation Status'),
            'title'     => Mage::helper('scandi_slider')->__('Slider Navigation Status'),
            'name'      => 'show_navigation',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('scandi_slider')->__('Enabled'),
                '0' => Mage::helper('scandi_slider')->__('Disabled'),
            ),
            'note'      => 'If Enabled, will show previous and next slide buttons'
        ));

        $fieldset->addField('lazy_load', 'select', array(
            'label'     => Mage::helper('scandi_slider')->__('Lazy loading status'),
            'title'     => Mage::helper('scandi_slider')->__('Lazy loading status'),
            'name'      => 'lazy_load',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('scandi_slider')->__('Enabled'),
                '0' => Mage::helper('scandi_slider')->__('Disabled'),
            ),
            'note'      => 'If Enabled, Images will be loaded on demand, this option can improve page loading times, but slider won\'t be visible while page is being rendered.'
        ));

        $fieldset->addField('animation_speed', 'text', array(
            'name'      => 'animation_speed',
            'label'     => Mage::helper('scandi_slider')->__('Animation Speed'),
            'title'     => Mage::helper('scandi_slider')->__('Animation Speed'),
            'required'  => false,
            'note'      => 'Default - 500ms, write value as numbers only, in milliseconds (1 second is 1000ms)'
        ));

        $fieldset->addField('slide_speed', 'text', array(
            'name'      => 'slide_speed',
            'label'     => Mage::helper('scandi_slider')->__('Autoplay Speed'),
            'title'     => Mage::helper('scandi_slider')->__('Autoplay Speed'),
            'required'  => false,
            'note'      => 'Default - 5s, write value as numbers only, in seconds. Set how long each slide is displayed.'
        ));

        $fieldset->addField('slides_to_display', 'text', array(
            'name'      => 'slides_to_display',
            'label'     => Mage::helper('scandi_slider')->__('Number of slides to display'),
            'title'     => Mage::helper('scandi_slider')->__('Number of slides to display'),
            'required'  => false,
            'note'      => 'Default - 1, Number of slides displayed simultaneously. useful for brand logos carousels, where you want to display more logos at the same time.'
        ));

        $fieldset->addField('slides_to_scroll', 'text', array(
            'name'      => 'slides_to_scroll',
            'label'     => Mage::helper('scandi_slider')->__('Number of slides to scroll'),
            'title'     => Mage::helper('scandi_slider')->__('Number of slides to scroll'),
            'required'  => false,
            'note'      => 'Default - 1, Number of slides to be scrolled at the same time. E.g. if you display 2 slides simultaneously you would want to change both or only 1 of them.'
        ));

        $fieldset->addField('slides_to_display_tablet', 'text', array(
            'name'      => 'slides_to_display_tablet',
            'label'     => Mage::helper('scandi_slider')->__('Number of slides to display on tablets'),
            'title'     => Mage::helper('scandi_slider')->__('Number of slides to display on tablets'),
            'required'  => false,
            'note'      => 'Default - 1, Number of Slides displayed simultaneously on tablet devices'
        ));

        $fieldset->addField('slides_to_scroll_tablet', 'text', array(
            'name'      => 'slides_to_scroll_tablet',
            'label'     => Mage::helper('scandi_slider')->__('Number of slides to scroll on tablets'),
            'title'     => Mage::helper('scandi_slider')->__('Number of slides to scroll on tablets'),
            'required'  => false,
            'note'      => 'Default - 1, Number of slides to be scrolled at the same time. E.g. if you display 2 slides simultaneously you would want to change both or only 1 of them.'
        ));

        $fieldset->addField('slides_to_display_mobile', 'text', array(
            'name'      => 'slides_to_display_mobile',
            'label'     => Mage::helper('scandi_slider')->__('Number of slides to display on mobile'),
            'title'     => Mage::helper('scandi_slider')->__('Number of slides to display on mobile'),
            'required'  => false,
            'note'      => 'Default - 1, Number of Slides displayed simultaneously on mobile devices'
        ));

        $fieldset->addField('slides_to_scroll_mobile', 'text', array(
            'name'      => 'slides_to_scroll_mobile',
            'label'     => Mage::helper('scandi_slider')->__('Number of slides to scroll on mobile'),
            'title'     => Mage::helper('scandi_slider')->__('Number of slides to scroll on mobile'),
            'required'  => false,
            'note'      => 'Default - 1, Number of slides to be scrolled at the same time. E.g. if you display 2 slides simultaneously you would want to change both or only 1 of them.'
        ));

        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        Mage::dispatchEvent(
            'adminhtml_cms_slider_edit_tab_main_prepare_form',
            array('form' => $form)
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('scandi_slider')->__('General Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('scandi_slider')->__('General Information');
    }

    /**
     * Returns tab's status flag - can be shown or not
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns tab's status flag - hidden or not
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Map_Edit_Tab_Products_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('cmsSliderImageMapProductGrid');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare collection for grid
     *
     * @return Scandi_Slider_Block_Adminhtml_Slider_Image_Map_Edit_Tab_Products
     */
    public function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('visibility')
            ->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns
     *
     * @return Scandi_Slider_Block_Adminhtml_Slider_Image_Map_Edit_Tab_Products
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('catalog')->__('ID'),
            'sortable'  => true,
            'width'     => '60',
            'index'     => 'entity_id'
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('catalog')->__('Name'),
            'index'     => 'name'
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('catalog')->__('SKU'),
            'width'     => '140px',
            'index'     => 'sku'
        ));
        $this->addColumn('visibility', array(
            'header'    => Mage::helper('catalog')->__('Visibility'),
            'width'     => '140px',
            'index'     => 'visibility',
            'type'      => 'options',
            'options'   => Mage::getModel('catalog/product_visibility')->getOptionArray(),
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/editImageMap', array(
            'id'            => $this->getRequest()->getParam('id'),
            'image_id'      => $this->getRequest()->getParam('image_id'),
            'map_id'        => $this->getRequest()->getParam('map_id'),
            'product_id'    => $row->getId(),
            'active_tab'    => 'slider_image_map_page_products_section'
        ));
    }
}
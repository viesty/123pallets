<?php
    /**
     * @category    Scandi
     * @package     Scandi_Slider
     * @author      Livija Golosujeva <info@scandiweb.com>
     */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Map_Edit_Tab_Products
    extends Mage_Adminhtml_Block_Widget_Form
        implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        $model = Mage::registry('slider_slider_image_map');
        $productId = $this->getRequest()->getParam('product_id');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('base_fieldset', array(
                'legend' => Mage::helper('scandi_slider')->__('Product Information'))
        );

        $fieldset->addField('product_id', 'text', array(
            'name'      => 'product_id',
            'label'     => Mage::helper('scandi_slider')->__('Product ID'),
            'title'     => Mage::helper('scandi_slider')->__('Product ID'),
            'required'  => false,
            'value'     => $productId
        ));


        Mage::dispatchEvent(
            'adminhtml_cms_slider_image_map_edit_prepare_form',
            array('form' => $form)
        );

        if (!$productId) {
            $form->setValues($model->getData());
        }

        $fieldset->addField('link', 'link', array(
            'style'     => "",
            'href'      => $this->getUrl('*/*/updateImageMapProduct', array('_current'=>true)),
            'value'     => Mage::helper('scandi_slider')->__('Choose product')
        ));
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('scandi_slider')->__('Map Product Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('scandi_slider')->__('Map Product Information');
    }

    /**
     * Returns tab's status flag - can be shown or not
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns tab's status flag - hidden or not
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
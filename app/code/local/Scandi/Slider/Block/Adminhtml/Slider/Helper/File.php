<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Helper_File extends Varien_Data_Form_Element_Abstract
{
    /**
     * Construct and set type
     *
     * @param array $data
     */
    public function __construct($data)
    {
        parent::__construct($data);
        $this->setType('file');
    }

    /**
     * Create link for added image preview
     *
     * @return string
     */
    public function getElementHtml()
    {
        $html = '';
        $this->addClass('input-file');
        $html .= parent::getElementHtml();
        if ($this->getValue()) {
            $url = $this->_getUrl();
            if (!preg_match("/^http\:\/\/|https\:\/\//", $url)) {
                $url = Mage::getBaseUrl('media') . 'slider' . DS . $url;
            }
            $html .= '<br /><a href="' . $url . '">' . $this->_getUrl() . '</a> ';
        }
        return $html;
    }

    /**
     * Return value - image location
     *
     * @return mixed
     */
    protected function _getUrl()
    {
        return $this->getValue();
    }
}
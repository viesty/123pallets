<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('slider_page_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('scandi_slider')->__('Slider Information'));
    }

    /**
     * Add "Slider Images" tab and its content
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        if ($this->getRequest()->getParam('id')) {
            $imagesTabContent = $this->getLayout()
                ->createBlock('scandi_slider/adminhtml_slider_edit_tab_images')
                ->toHtml();
        } else {
            $imagesTabContent = Mage::helper('scandi_slider')->__(
                '<ul class="messages"><li class="notice-msg"><ul><li><span>%s</span></li></ul></li></ul>',
                Mage::helper('scandi_slider')->__('You will be able to manage images after saving this slider.')
            );
        }

        $imageSectionStatus = $this->getRequest()->getParam('active_tab') == 'slider_page_tabs_images_section'
            || $this->getRequest()->getParam('page') || $this->getRequest()->getParam('limit')
            || $this->getRequest()->getParam('filter') || $this->getRequest()->getParam('sort');

        $this->addTab('images_section', array(
            'label'     => $this->__('Slider Images'),
            'title'     => $this->__('Slider Images'),
            'active'    => $imageSectionStatus,
            'content'   => $imagesTabContent,
        ));

        return parent::_beforeToHtml();
    }
}
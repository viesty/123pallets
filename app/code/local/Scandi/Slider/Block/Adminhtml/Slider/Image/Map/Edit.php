<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Map_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId   = 'id';
        $this->_blockGroup = 'scandi_slider';
        $this->_controller = 'adminhtml_slider_image_map';

        parent::__construct();

        $this->_updateButton('back', 'onclick', 'setLocation(\'' . $this->_getBackToImageUrl() . '\')');

        if (Mage::registry('slider_slider_image_map')) {
            $this->_updateButton('delete', 'onclick', 'setLocation(\'' . $this->_getDeleteUrl() . '\')');
        }

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Retrieve text for header element depending on loaded page
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('slider_slider_image_map')->getId()) {
            return Mage::helper('scandi_slider')->__("Edit Image Map '%s'",
                $this->escapeHtml(Mage::registry('slider_slider_image_map')->getMapTitle()));
        } else {
            return Mage::helper('scandi_slider')->__('New Image Map');
        }
    }

    /**
     * Getter of url for "Back" button
     *
     * @return string
     */
    protected function _getBackToImageUrl()
    {
        return $this->getUrl('*/*/updateImageMap', array(
            'id'        => $this->getRequest()->getParam('id'),
            'image_id'  => $this->getRequest()->getParam('image_id')
        ));
    }

    /**
     * Getter of url for "Delete" button
     *
     * @return string
     */
    protected function _getDeleteUrl()
    {
        return $this->getUrl('*/*/deleteImageMap', array(
            'id'        => $this->getRequest()->getParam('id'),
            'image_id'  => $this->getRequest()->getParam('image_id'),
            'map_id'    => $this->getRequest()->getParam('map_id')
        ));
    }
}
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId   = 'id';
        $this->_blockGroup = 'scandi_slider';
        $this->_controller = 'adminhtml_slider_image';

        parent::__construct();

        $this->_updateButton('back', 'onclick', 'setLocation(\'' . $this->_getBackUrl() . '\')');
        $this->_updateButton('delete', 'onclick', 'setLocation(\'' . $this->_getDeleteUrl() . '\')');

        if ($this->getRequest()->getParam('image_id')) {
            $this->_addButton('update', array(
                'label'     => Mage::helper('scandi_slider')->__('Update Image Map'),
                'onclick'   => 'setLocation(\'' . $this->_getUpdateUrl() . '\')',
                'class'     => 'save',
            ), -100, 5);
        }
    }

    /**
     * Retrieve text for header element depending on loaded page
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('slider_slider_image')->getId()) {
            return Mage::helper('scandi_slider')->__("Edit Slider Image '%s'",
                $this->escapeHtml(Mage::registry('slider_slider_image')->getImageTitle()));
        } else {
            return Mage::helper('scandi_slider')->__('New Slider Image');
        }
    }

    /**
     * Getter of url for "Back" button
     *
     * @return string
     */
    protected function _getBackUrl()
    {
        return $this->getUrl('*/*/edit', array(
            'id'        => $this->getRequest()->getParam('id'),
            '_current'  => true
        ));
    }

    /**
     * Getter of url for "Delete" button
     *
     * @return string
     */
    protected function _getDeleteUrl()
    {
        return $this->getUrl('*/*/deleteImage', array(
            'id'        => $this->getRequest()->getParam('id'),
            'image_id'  => $this->getRequest()->getParam('image_id')
        ));
    }

    /**
     * Getter of url for "Update Image Map" button
     *
     * @return string
     */
    protected function _getUpdateUrl()
    {
        return $this->getUrl('*/*/updateImageMap', array(
            'id'        => $this->getRequest()->getParam('id'),
            'image_id'  => $this->getRequest()->getParam('image_id')
        ));
    }
}
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId   = 'id';
        $this->_controller = 'adminhtml_slider';
        $this->_blockGroup = 'scandi_slider';

        parent::__construct();

        $this->_addButton('saveandcontinue', array(
            'label'   => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick' => 'saveAndContinueEdit(\'' . $this->_getSaveAndContinueUrl() . '\')',
            'class'   => 'save',
        ), -100);

        if (Mage::registry('slider_slider')->getId()) {
            $this->_addButton('addsliderimage', array(
                'label'   => Mage::helper('scandi_slider')->__('Add Slider Image'),
                'onclick' => 'setLocation(\'' . $this->_getAddSliderImageUrl() . '\')',
                'class'   => 'add'
            ), 0);
        }

        $this->_formScripts[] = "
            function saveAndContinueEdit(urlTemplate) {
                var template = new Template(urlTemplate, /(^|.|\\r|\\n)({{(\w+)}})/),
                    tabsIdValue = slider_page_tabsJsTabs.activeTab.id,
                    url = template.evaluate({tab_id:tabsIdValue});

                editForm.submit(url);
            }
        ";
    }

    /**
     * Retrieve text for header element depending on loaded page
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('slider_slider')->getId()) {
            return Mage::helper('scandi_slider')->__("Edit Slider '%s'",
                $this->escapeHtml(Mage::registry('slider_slider')->getTitle()));
        } else {
            return Mage::helper('scandi_slider')->__('New Slider');
        }
    }

    /**
     * Getter of url for "Save and Continue" button
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
            'active_tab' => '{{tab_id}}'
        ));
    }

    /**
     * Getter of url for "Add Slider Image" button
     *
     * @return string
     */
    protected function _getAddSliderImageUrl()
    {
        $request = $this->getRequest();

        return $this->getUrl('*/*/newImage', array(
            'id'            => $request->getParam('id'),
            'active_tab'    => $request->getParam('active_tab'),
        ));
    }
}
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Map_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('image_map_page_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('scandi_slider')->__('Image Map Information'));
    }

    /**
     * Add "Image Map Product" tab and its content
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        if ($this->getRequest()->getParam('map_id')) {
            $productTabContent = $this->getLayout()
                ->createBlock('scandi_slider/adminhtml_slider_image_map_edit_tab_products')
                ->toHtml();
        } else {
            $productTabContent = Mage::helper('scandi_slider')->__(
                '<ul class="messages"><li class="notice-msg"><ul><li><span>%s</span></li></ul></li></ul>',
                Mage::helper('scandi_slider')->__('You will be able to add product after saving this map.')
            );
        }

        $this->addTab('slider_image_map_page_products_section', array(
            'label'     => $this->__('Image Map Product'),
            'title'     => $this->__('Image Map Product'),
            'content'   => $productTabContent,
        ));

        return parent::_beforeToHtml();
    }
}
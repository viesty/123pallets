<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Map_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('cmsSliderImageMapGrid');
    }

    public function _prepareCollection()
    {
        $collection = Mage::getModel('scandi_slider/image_map')->getResourceCollection()
            ->addImageFilter(Mage::registry('slider_slider_image'));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'   => Mage::helper('scandi_slider')->__('ID'),
            'index'    => 'id',
            'width'    => '10px'
        ));

        $this->addColumn('map_title', array(
            'header'   => Mage::helper('scandi_slider')->__('Image Map Title'),
            'index'    => 'map_title',
            'width'    => '300px'
        ));

        $this->addColumn('is_active', array(
            'header'   => Mage::helper('scandi_slider')->__('Active'),
            'index'    => 'is_active',
            'width'    => '20px'
        ));
    }

    /**
     * Return row url
     *
     * @param Scandi_Slider_Model_Slider $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/editImageMap', array(
            'id'        => $this->getRequest()->getParam('id'),
            'image_id'  => $row->getImageId(),
            'map_id'    => $row->getId()
        ));
    }
}
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    protected function _prepareForm()
    {
        /* @var $model Scandi_Slider_Model_Image*/
        $model = Mage::registry('slider_slider_image');
        $sliderId = $this->getRequest()->getParam('id');

        $form = new Varien_Data_Form(array(
            'method'    => 'post',
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/saveImage', array('id' => $sliderId)),
            'enctype'   => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
                'legend' => Mage::helper('scandi_slider')->__('General Information'))
        );
        $fieldset->addType('file', Mage::getConfig()->getBlockClassName('scandi_slider/adminhtml_slider_helper_file'));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name'  => 'id',
            ));
        }

        if ($model->getSliderId()) {
            $fieldset->addField('slider_id', 'hidden', array(
                'name'  => 'slider_id',
            ));
        }

        $fieldset->addField('image_title', 'text', array(
            'name'      => 'image_title',
            'label'     => Mage::helper('scandi_slider')->__('Image Title'),
            'title'     => Mage::helper('scandi_slider')->__('Image Title'),
            'required'  => true
        ));

        $fieldset->addField('image_is_active', 'select', array(
            'label'     => Mage::helper('scandi_slider')->__('Status'),
            'title'     => Mage::helper('scandi_slider')->__('Image Status'),
            'name'      => 'image_is_active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('scandi_slider')->__('Enabled'),
                '0' => Mage::helper('scandi_slider')->__('Disabled'),
            ),
        ));

        $fieldset->addField('location', 'file', array(
            'label'     => Mage::helper('scandi_slider')->__('Image'),
            'required'  => false,
            'name'      => 'location',
            'note'      => 'Only JPG, JPEG, PNG',
        ));

        $fieldset->addField('iframe', 'text', array(
            'label'     => Mage::helper('scandi_slider')->__('iframe video code'),
            'required'  => false,
            'name'      => 'iframe',
            'note'      => 'Copy youtube or vimeo or any iframe code from "share" tab here if you want to display video in slide.',
        ));
        
        $fieldset->addField('image_position', 'text', array(
            'name'      => 'image_position',
            'label'     => Mage::helper('scandi_slider')->__('Position'),
            'title'     => Mage::helper('scandi_slider')->__('Position'),
            'required'  => false,
            'class'     => 'validate-digits'
        ));

        $fieldset->addField('slide_link', 'text', array(
            'name'      => 'slide_link',
            'label'     => Mage::helper('scandi_slider')->__('Slide as hyperlink'),
            'title'     => Mage::helper('scandi_slider')->__('Slide as hyperlink'),
            'required'  => false,
            'note'      => 'Full URL where slide should redirect after click on it.'
        ));

        $fieldset->addField('slide_title', 'text', array(
            'name'      => 'slide_title',
            'label'     => Mage::helper('scandi_slider')->__('Slide title'),
            'title'     => Mage::helper('scandi_slider')->__('Slide title'),
            'required'  => false,
            'note'      => 'Title for text which will appear on slide'
        ));

        $fieldset->addField('slide_text', 'text', array(
            'name'      => 'slide_text',
            'label'     => Mage::helper('scandi_slider')->__('Slide text'),
            'title'     => Mage::helper('scandi_slider')->__('Slide text'),
            'required'  => false,
            'note'      => 'Text which will appear on slide'
        ));

        Mage::dispatchEvent(
            'adminhtml_cms_slider_image_edit_prepare_form',
            array('form' => $form)
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
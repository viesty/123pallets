<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Map extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_slider_image_map';
        $this->_blockGroup = 'scandi_slider';
        $this->_headerText = Mage::helper('scandi_slider')->__("Manage '%s' Map",
            $this->escapeHtml(Mage::registry('slider_slider_image')->getImageTitle()));

        parent::__construct();

        $this->_addButton('back', array(
            'label'     => Mage::helper('scandi_slider')->__('Back to Image'),
            'onclick'   => 'setLocation(\'' . $this->_getBackToImageUrl() . '\')',
            'class'     => 'back',
        ), -1, 5);

        $this->_addButton('add_image_map', array(
            'label'     => Mage::helper('scandi_slider')->__('Add New Image Map'),
            'onclick'   => 'setLocation(\'' . $this->_getAddSliderImageMapUrl() . '\')',
            'class'     => 'add',
        ), 0, 5);

        $this->_removeButton('add');
    }

    /**
     * Getter of url for "Back to Image" button
     *
     * @return string
     */
    protected function _getBackToImageUrl()
    {
        return $this->getUrl('*/*/editImage', array(
            'id'        => $this->getRequest()->getParam('id'),
            'image_id'  => $this->getRequest()->getParam('image_id'),
            '_current'  => true
        ));
    }

    /**
     * Getter of url for "Add Slider Image Map" button
     *
     * @return string
     */
    protected function _getAddSliderImageMapUrl()
    {
        $request = $this->getRequest();

        return $this->getUrl('*/*/editImageMap', array(
            'id'            => $request->getParam('id'),
            'image_id'      => $this->getRequest()->getParam('image_id'),
            'active_tab'    => $request->getParam('active_tab')
        ));
    }
}
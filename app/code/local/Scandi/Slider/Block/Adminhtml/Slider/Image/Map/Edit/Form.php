<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Map_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $imageId = $this->getRequest()->getParam('image_id');
        $sliderId = $this->getRequest()->getParam('id');

        $form = new Varien_Data_Form(array(
            'method'    => 'post',
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/saveImageMap', array('id' => $sliderId, 'image_id' => $imageId)),
            'enctype'   => 'multipart/form-data'
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
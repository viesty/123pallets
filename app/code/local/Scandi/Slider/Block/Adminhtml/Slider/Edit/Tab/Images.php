<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Edit_Tab_Images extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('cmsSliderImagesGrid');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare collection for grid
     *
     * @return Scandi_Slider_Block_Adminhtml_Slider_Edit_Tab_Images
     */
    protected function _prepareCollection()
    {
        /* @var $collection Scandi_Slider_Model_Resource_Image_Collection */
        $collection = Mage::getModel('scandi_slider/image')->getResourceCollection()
            ->addSliderFilter(Mage::registry('slider_slider'));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns
     *
     * @return Scandi_Slider_Block_Adminhtml_Slider_Edit_Tab_Images
     */
    protected function _prepareColumns()
    {
        $this->addColumn('image_title', array(
            'header'    => Mage::helper('scandi_slider')->__('Image Title'),
            'index'     => 'image_title',
        ));

        $this->addColumn('image_is_active', array(
            'header'    => Mage::helper('scandi_slider')->__('Status'),
            'index'     => 'image_is_active',
            'type'      => 'options',
            'options'   => array(
                0 => Mage::helper('scandi_slider')->__('Disabled'),
                1 => Mage::helper('scandi_slider')->__('Enabled')
            ),
        ));

        $this->addColumn('image_position', array(
            'header'    => Mage::helper('scandi_slider')->__('Position'),
            'index'     => 'image_position',
            'width'     => '1'
        ));

        return parent::_prepareColumns();
    }

    /**
     * Return row url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/editImage', array(
            'id'            => $this->getRequest()->getParam('id'),
            'image_id'      => $row->getId(),
            'active_tab'    => 'slider_page_tabs_images_section',
        ));
    }
}
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('cmsSliderGrid');
    }

    public function _prepareCollection()
    {
        $collection = Mage::getModel('scandi_slider/slider')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'   => Mage::helper('scandi_slider')->__('ID'),
            'index'    => 'id',
            'width'    => '10px'
        ));

        $this->addColumn('block_id', array(
            'header'   => Mage::helper('scandi_slider')->__('Slider Block ID'),
            'index'    => 'block_id',
            'width'    => '200px'
        ));

        $this->addColumn('title', array(
            'header'   => Mage::helper('scandi_slider')->__('Slider Title'),
            'index'    => 'title',
            'width'    => '300px'
        ));

        $this->addColumn('is_active', array(
            'header'   => Mage::helper('scandi_slider')->__('Active'),
            'index'    => 'is_active',
            'width'    => '20px'
        ));
    }

    /**
     * Return row url
     *
     * @param Scandi_Slider_Model_Slider $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
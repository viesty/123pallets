<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Block_Adminhtml_Slider_Image_Map_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        $model = Mage::registry('slider_slider_image_map');
        $imageId = $this->getRequest()->getParam('image_id');
        $sliderId = $this->getRequest()->getParam('id');
        $imageModel = Mage::registry('slider_slider_image');
        $imageUrl = Mage::getBaseUrl('media') . 'slider' . DS . $imageModel->getLocation();

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('slider_image_map_');

        $fieldset = $form->addFieldset('base_fieldset', array(
                'legend' => Mage::helper('scandi_slider')->__('General Information'))
        );

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name'  => 'id',
            ));
        }

        if ($imageId) {
            $fieldset->addField('image_id', 'hidden', array(
                'name'  => 'image_id',
            ));
        }

        $fieldset->addField('map_title', 'text', array(
            'name'      => 'map_title',
            'label'     => Mage::helper('scandi_slider')->__('Image Map Title'),
            'title'     => Mage::helper('scandi_slider')->__('Image Map Title'),
            'required'  => true
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('scandi_slider')->__('Status'),
            'title'     => Mage::helper('scandi_slider')->__('Image Map Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('scandi_slider')->__('Enabled'),
                '0' => Mage::helper('scandi_slider')->__('Disabled'),
            ),
        ));

        $fieldset->addField('coordinates', 'textarea', array(
            'name'      => 'coordinates',
            'label'     => Mage::helper('scandi_slider')->__('Image Map Coordinates'),
            'title'     => Mage::helper('scandi_slider')->__('Image Map Coordinates'),
            'required'  => true,
            'class'     => 'canvas-area',
            'style'     => 'margin-bottom:10px; width:275px; height:100px;',
            'readonly' => true,
        ));

        Mage::dispatchEvent(
            'adminhtml_cms_slider_image_map_edit_prepare_form',
            array('form' => $form)
        );

        $form->setValues($model->getData());

        //Hidden field for image url
        if ($imageId) {
            $fieldset->addField('image_location', 'hidden', array(
                'name'  => 'image_location',
                'value' => $imageUrl
            ));
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('scandi_slider')->__('Map General Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('scandi_slider')->__('Map General Information');
    }

    /**
     * Returns tab's status flag - can be shown or not
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns tab's status flag - hidden or not
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
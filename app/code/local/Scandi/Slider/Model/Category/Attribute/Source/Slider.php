<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Ramona Cunska <info@scandiweb.com>
 */
class Scandi_Slider_Model_Category_Attribute_Source_Slider extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = Mage::getModel('scandi_slider/slider')->toOptionArray();
            array_unshift($this->_options, array(
                'value' => 0,
                'label' => Mage::helper('scandi_slider')->__('-- Please Select --')
            ));
        }

        return $this->_options;
    }
}
<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Model_Image_Map extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('scandi_slider/image_map');
    }
}
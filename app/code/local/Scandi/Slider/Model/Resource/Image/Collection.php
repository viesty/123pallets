<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Model_Resource_Image_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('scandi_slider/image');
    }

    /**
     * Add slider filter to image collection
     *
     * @param   int | Scandi_Slider_Model_Slider $slider
     * @return  Scandi_Slider_Model_Resource_Image_Collection
     */
    public function addSliderFilter($slider)
    {
        if ($slider instanceof Scandi_Slider_Model_Slider) {
            $slider = $slider->getId();
        }

        $this->addFilter('slider_id', $slider);

        return $this;
    }

    /**
     * Add status filter to image collection
     *
     * @return  Scandi_Slider_Model_Resource_Image_Collection
     */
    public function addStatusFilter()
    {
        $this->addFilter('image_is_active', 1);

        return $this;
    }
}
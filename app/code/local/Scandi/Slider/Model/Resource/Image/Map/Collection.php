<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Model_Resource_Image_Map_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('scandi_slider/image_map');
    }

    /**
     * Add slider filter to image map collection
     *
     * @param int | Scandi_Slider_Model_Slider $slider
     * @return Scandi_Slider_Model_Resource_Image_Map_Collection
     */
    public function addSliderFilter($slider)
    {
        if ($slider instanceof Scandi_Slider_Model_Slider) {
            $slider = $slider->getId();
        }

        $this->getSelect()->join(
            array('image_table' => $this->getTable('scandi_slider/image')),
            'main_table.image_id = image_table.id'
            . ' AND ' . $this->getConnection()->quoteInto('image_table.slider_id = ?', $slider),
            array()
        );

        return $this;
    }

    /**
     * Add image filter to image map collection
     *
     * @param   int | Scandi_Slider_Model_Image $image
     * @return  Scandi_Slider_Model_Resource_Image_Map_Collection
     */
    public function addImageFilter($image)
    {
        if ($image instanceof Scandi_Slider_Model_Image) {
            $image = $image->getId();
        }

        $this->addFieldToFilter('image_id', $image);

        return $this;
    }

    /**
     * Add status filter to image map collection
     *
     * @return  Scandi_Slider_Model_Resource_Image_Map_Collection
     */
    public function addStatusFilter()
    {
        $this->addFieldToFilter('is_active', 1);

        return $this;
    }
}
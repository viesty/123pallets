<?php
/**
 * @category    Scandi
 * @package     Scandi_Slider
 * @author      Livija Golosujeva <info@scandiweb.com>
 */
class Scandi_Slider_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Scandi_Slider_Adminhtml_IndexController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/scandi_slider')
            ->_addBreadcrumb(
                Mage::helper('scandi_slider')->__('CMS'),
                Mage::helper('scandi_slider')->__('CMS')
            )
            ->_addBreadcrumb(
                Mage::helper('scandi_slider')->__('Slider'),
                Mage::helper('scandi_slider')->__('Slider')
            );

        $this->_title($this->__('CMS'))
            ->_title($this->__('Slider'));

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Slider create action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Slider edit action
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('scandi_slider/slider');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('scandi_slider')->__('This slider no longer exists.'));

                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getTitle() : $this->__('New Slider'));
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('slider_slider', $model);

        $editSlider = Mage::helper('scandi_slider')->__('Edit Slider');
        $newSlider = Mage::helper('scandi_slider')->__('New Slider');

        $this->_initAction()->_addBreadcrumb(
            $id ? $editSlider : $newSlider,
            $id ? $editSlider : $newSlider
        );

        $this->renderLayout();
    }

    /**
     * Slider save action
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            /* @var $model Scandi_Slider_Model_Slider */
            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('scandi_slider/slider')->load($id);

            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')
                    ->addError(Mage::helper('scandi_slider')->__('This slider no longer exists.'));

                $this->_redirect('*/*/');
                return;
            }

            $model->setData($data);

            try {
                $model->save();
                Mage::getSingleton('adminhtml/session')
                    ->addSuccess(Mage::helper('scandi_slider')->__('The slider has been saved.'));

                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array(
                        'id'        => $model->getId(),
                        '_current'  => true
                    ));

                    return;
                }

                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Slider delete action
     */
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                /* @var $model Scandi_Slider_Model_Slider */
                $model = Mage::getModel('scandi_slider/slider')->load($id);
                $model->delete();

                Mage::getSingleton('adminhtml/session')
                    ->addSuccess(Mage::helper('scandi_slider')->__('The slider has been deleted.'));

                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }

        Mage::getSingleton('adminhtml/session')
            ->addError(Mage::helper('scandi_slider')->__('Unable to find a slider to delete.'));

        $this->_redirect('*/*/');
    }

    /**
     * Create new slider image
     */
    public function newImageAction()
    {
        $this->_forward('editImage');
    }

    /**
     * Slider image edit action
     */
    public function editImageAction()
    {
        /* @var $model Scandi_Slider_Model_Image */
        $id = $this->getRequest()->getParam('image_id');
        $model = Mage::getModel('scandi_slider/image');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('scandi_slider')->__('This slider image does not exist.')
                );

                $this->_redirectToSliderPage();
                return;
            }
        }

        $this->_title($model->getId() ? $model->getImageTitle() : $this->__('New Slider'));
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('slider_slider_image', $model);

        $editSliderImage = Mage::helper('scandi_slider')->__('Edit Slider Image');
        $newSliderImage = Mage::helper('scandi_slider')->__('New Slider Image');

        $this->_initAction()->_addBreadcrumb(
            $id ? $editSliderImage : $newSliderImage,
            $id ? $editSliderImage : $newSliderImage
        );

        $this->renderLayout();
    }

    /**
     * Slider Image save action
     */
    public function saveImageAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            /* @var $model Scandi_Slider_Model_Image */
            $id = $this->getRequest()->getParam('image_id');
            $sliderId = $this->getRequest()->getParam('id');
            $model = Mage::getModel('scandi_slider/image')->load($id);

            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')
                    ->addError(Mage::helper('scandi_slider')->__('This slider image no longer exists.'));

                $this->_redirectToSliderPage();
                return;
            }

            if (!$sliderId) {
                Mage::getSingleton('adminhtml/session')
                    ->addError(Mage::helper('scandi_slider')->__('Parent slider could not be found.'));

                $this->_redirectToSliderPage();
                return;
            }

            $data['slider_id'] = $sliderId;
            $model->setData($data);

            try {
                if (isset($_FILES['location']['name']) && (file_exists($_FILES['location']['tmp_name']))) {
                    $uploader = new Varien_File_Uploader('location');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','png'));

                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);

                    $path = Mage::getBaseDir('media') . DS . 'slider' . DS ;

                    $uploader->save($path, $_FILES['location']['name']);

                    $data['location'] = $_FILES['location']['name'];

                    $model->setData($data);
                }
                $model->save();
                Mage::getSingleton('adminhtml/session')
                    ->addSuccess(Mage::helper('scandi_slider')->__('The slider image has been saved.'));

                Mage::getSingleton('adminhtml/session')->setFormData(false);

                $this->_redirectToSliderPage();
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirectToImagePage();
                return;
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Slider image delete action
     */
    public function deleteImageAction()
    {
        if ($id = $this->getRequest()->getParam('image_id')) {
            try {
                /* @var $model Scandi_Slider_Model_Image */
                $model = Mage::getModel('scandi_slider/image')->load($id);
                $model->delete();

                Mage::getSingleton('adminhtml/session')
                    ->addSuccess(Mage::helper('scandi_slider')->__('The slider image has been deleted.'));

                $this->_redirectToSliderPage();
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

                $this->_redirectToImagePage();
                return;
            }
        }

        Mage::getSingleton('adminhtml/session')
            ->addError(Mage::helper('scandi_slider')->__('Unable to find a slider image to delete.'));

        $this->_redirectToSliderPage();
    }

    /**
     * Slider Image Map update action - view image map grid
     */
    public function updateImageMapAction()
    {
        $id = $this->getRequest()->getParam('image_id');
        $model = Mage::getModel('scandi_slider/image');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('scandi_slider')->__('Slider image does not exist.')
                );

                $this->_redirectToSliderPage();
                return;
            }
        }

        Mage::register('slider_slider_image', $model);

        $this->_title($model->getImageTitle());

        $updateSliderImageMap = Mage::helper('scandi_slider')->__('Update Slider Image Map');

        $this->_initAction()->_addBreadcrumb(
            $updateSliderImageMap,
            $updateSliderImageMap
        );

        $this->renderLayout();
    }

    /**
     * Create new Slider Image Map
     */
    public function newImageMapAction()
    {
        $this->_forward('editImageMap');
    }

    /**
     * Slider Image Map edit action
     */
    public function editImageMapAction()
    {
        $id = $this->getRequest()->getParam('map_id');
        $imageId = $this->getRequest()->getParam('image_id');
        $model = Mage::getModel('scandi_slider/image_map');
        $imageModel = Mage::getModel('scandi_slider/image')->load($imageId);

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('scandi_slider')->__('This image map does not exist.')
                );

                $this->_redirectToImagePage();
                return;
            }
        }

        $this->_title($model->getId() ? $model->getMapTitle() : $this->__('New Image Map'));
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('slider_slider_image_map', $model);
        Mage::register('slider_slider_image', $imageModel);

        $editImageMap = Mage::helper('scandi_slider')->__('Edit Image Map');
        $newImageMap = Mage::helper('scandi_slider')->__('New Image Map');

        $this->_initAction()->_addBreadcrumb(
            $id ? $editImageMap : $newImageMap,
            $id ? $editImageMap : $newImageMap
        );

        $this->renderLayout();
    }

    /**
     * Slider Image Map save action
     */
    public function saveImageMapAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $id = $this->getRequest()->getParam('map_id');
            $imageId = $this->getRequest()->getParam('image_id');
            $sliderId = $this->getRequest()->getParam('id');
            $model = Mage::getModel('scandi_slider/image_map')->load($id);

            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')
                    ->addError(Mage::helper('scandi_slider')->__('This map no longer exists.'));

                $this->_redirectToImageMapPage();
                return;
            }

            $data['image_id'] = $imageId;
            if (isset($data['product_id']) && intVal($data['product_id'], 10) <= 0) {
                $data['product_id'] = new Zend_Db_Expr('NULL');
            }
            $model->setData($data);

            try {
                $model->save();
                Mage::getSingleton('adminhtml/session')
                    ->addSuccess(Mage::helper('scandi_slider')->__('Image map has been saved.'));

                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/editImageMap', array(
                        'id'        => $sliderId,
                        'image_id'  => $model->getImageId(),
                        'map_id'    => $model->getId(),
                        '_current'  => true
                    ));

                    return;
                }

                $this->_redirectToImageMapPage();
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirect('*/*/editImageMap', array(
                    'id'        => $sliderId,
                    'image_id'  => $imageId
                ));
                return;
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Image map delete action
     */
    public function deleteImageMapAction()
    {
        if ($id = $this->getRequest()->getParam('map_id')) {
            try {
                /* @var $model Scandi_Slider_Model_Image */
                $model = Mage::getModel('scandi_slider/image_map')->load($id);
                $model->delete();

                Mage::getSingleton('adminhtml/session')
                    ->addSuccess(Mage::helper('scandi_slider')->__('Image map has been deleted.'));

                $this->_redirectToImageMapPage();
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

                $this->_redirectToImageMapPage();
                return;
            }
        }

        Mage::getSingleton('adminhtml/session')
            ->addError(Mage::helper('scandi_slider')->__('Unable to find an image map to delete.'));

        $this->_redirectToImageMapPage();
    }

    /**
     * Image Map Product grid action
     */
    public function updateImageMapProductAction()
    {
        $id = $this->getRequest()->getParam('map_id');
        $model = Mage::getModel('scandi_slider/image_map')->load($id);

        if (!$model->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('scandi_slider')->__('This image map does not exist.')
            );

            $this->_redirectToImagePage();
            return;
        }

        $this->_title($model->getImageTitle());

        $updateSliderImageMap = Mage::helper('scandi_slider')->__('Update Slider Image Map');

        $this->_initAction()->_addBreadcrumb(
            $updateSliderImageMap,
            $updateSliderImageMap
        );

        $this->renderLayout();
    }

    /**
     * Redirects to parent slider edit page
     */
    protected function _redirectToSliderPage()
    {
        $this->_redirect('*/*/edit', array(
            'id' => $this->getRequest()->getParam('id'),
            'active_tab' => 'slider_page_tabs_images_section',
        ));
    }

    /**
     * Redirects to image edit page
     */
    protected function _redirectToImagePage()
    {
        $this->_redirect('*/*/editImage', array(
            'id' => $this->getRequest()->getParam('id'),
            'image_id' => $this->getRequest()->getParam('image_id')
        ));
    }

    /**
     * Redirects to image map edit page
     */
    protected function _redirectToImageMapPage()
    {
        $this->_redirect('*/*/updateImageMap', array(
            'id' => $this->getRequest()->getParam('id'),
            'image_id' => $this->getRequest()->getParam('image_id')
        ));
    }
}
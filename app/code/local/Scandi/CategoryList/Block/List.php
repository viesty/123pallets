<?php

/**
 * Scandi_CategoryList
 *
 * @category    Scandi
 * @package     CategoryList
 * @author      Viesturs Ružāns <viestursr@majaslapa.lv>
 * @copyright   Copyright (c) 2015
 * @license     [license url]
 *
 */

/**
 * Class Scandi_CategoryList_Block_List
 */
class Scandi_CategoryList_Block_List extends Mage_Core_Block_Template
{

    function _construct()
    {
        $this->setTemplate("scandi/categorylist/list.phtml");
    }

    public function getParentCategory()
    {
        return $this->getData('category_id');
    }

    public function getChildrenCategories()
    {
        /**
         * Child category id's
         * All of parents' child category id's in a string
         * @var ids
         */
        $ids = Mage::getModel('catalog/category')->load($this->getParentCategory())->getChildren();
        if (!empty($ids)) {
            return explode(',', $ids);
        } else {
            return false;
        }
    }

    public function getName($categoryId)
    {
        return Mage::getModel('catalog/category')->load($categoryId)->getName();
    }

    public function getUrl($categoryId)
    {
        return Mage::getModel('catalog/category')->load($categoryId)->getCategoryIdUrl();
    }
}